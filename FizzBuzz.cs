using System;

namespace fizzbuzz
{
    public class FizzBuzz
    {
        public FizzBuzz() {}

        public void DoFizzBuzz(int upTo)
        {
            for (int i = 1; i <= upTo; i++)
            {
                if (!(CheckDoFizz(i) | CheckDoBuzz(i)))
                {
                    Console.Write(i);
                }
                Console.Write(" ");
            }
        }

        private bool CheckDoFizz(int i)
        {
            if (i % 3 == 0)
            {
                Console.Write("Fizz");
                return true;
            }

            return false;
        }

        private bool CheckDoBuzz(int i)
        {
            if (i % 5 == 0)
            {
                Console.Write("Buzz");
                return true;
            }
        
            return false;
        }
    }
}